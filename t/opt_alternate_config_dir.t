use 5.010_000;
use strict;
use utf8;
use Glib qw(TRUE FALSE);
use Test::More tests => 7;
use Test::Exception;

require_ok ('Clawsker');

use Clawsker;

ok ( defined &Clawsker::opt_alternate_config_dir, 'has function' );

like ( $Clawsker::CONFIGDIR, qr{^.*\.claws-mail$}, 'init: dir' );
is ( $Clawsker::ALTCONFIGDIR, FALSE, 'init: alt' );

dies_ok { Clawsker::opt_alternate_config_dir('', 'invalid') } 'invalid dir';

Clawsker::opt_alternate_config_dir('', '.');

is ( $Clawsker::CONFIGDIR, '.', 'valid: dir' );
is ( $Clawsker::ALTCONFIGDIR, TRUE, 'valid: alt' );
